const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");

router.post("/", auth.verify, (req, res) => {
  let verifyIsAdmin = auth.decode(req.headers.authorization).isAdmin;
  if (verifyIsAdmin) {
    const data = {
      course: req.body,
      isAdmin: verifyIsAdmin,
    };
    courseController
      .addCourse(data)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.send(verifyIsAdmin);
  }
});

router.get("/allCourses", (req, res) => {
  courseController
    .getAllCourses()
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/activeCourses", (req, res) => {
  courseController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/:courseId", (req, res) => {
  courseController
    .getCourse(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

router.put("/:courseId", auth.verify, (req, res) => {
  const data = auth.decode(req.headers.authorization).isAdmin;
  if (data) {
    courseController
      .updateCourse(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send(false);
  }
});

router.patch("/:courseId/archive", auth.verify, (req, res) => {
  const data = auth.decode(req.headers.authorization).isAdmin;
  if (data) {
    courseController
      .archiveCourse(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    return res.send(false);
  }
});

module.exports = router;
