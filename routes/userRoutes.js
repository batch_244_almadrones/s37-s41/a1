const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
// const user = require("../models/user.js");
const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  userController
    .getProfile(userData.id)
    .then((resultFromCOntroller) => res.send(resultFromCOntroller));
});

router.post("/enroll", auth.verify, (req, res) => {
  let loggedUser = auth.decode(req.headers.authorization);

  let data = {
    userId: loggedUser.id,
    courseId: req.body.courseId,
  };

  // console.log(loggedUser.isAdmin);
  // console.log(loggedUser.id == data.userId);
  if (loggedUser.isAdmin === false) {
    userController
      .enroll(data)
      .then((resultFromCOntroller) => res.send(resultFromCOntroller));
  } else {
    return res.send(false);
  }
});

module.exports = router;
