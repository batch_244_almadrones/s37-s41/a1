const Course = require("../models/Course.js");

module.exports.addCourse = (data) => {
  let newCourse = new Course({
    name: data.course.name,
    description: data.course.description,
    price: data.course.price,
    isActive: data.course.isActive,
  });
  return newCourse.save().then((course, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.getAllCourses = () => {
  return Course.find({}).then((res) => {
    return res;
  });
};

module.exports.getAllActive = () => {
  return Course.find({ isActive: true }).then((res) => {
    return res;
  });
};

module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams.courseId).then((res) => {
    return res;
  });
};

module.exports.updateCourse = (reqParams, reqBody) => {
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
    (course, err) => {
      if (err) {
        return false;
      } else {
        return course;
      }
    }
  );
};

module.exports.archiveCourse = (reqParams, reBody) => {
  let data = {
    isActive: false,
  };
  return Course.findByIdAndUpdate(reqParams.courseId, data).then(
    (course, err) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    }
  );
};
