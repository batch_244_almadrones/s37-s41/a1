const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/Course.js");
const User = require("../models/User.js");

// let findEmail = (bodyRequest) => {
//   return User.findOne({ email: bodyRequest.email }).then((result) => {
//     if (result != null) {
//       return true;
//     } else {
//       return false;
//     }
//   });
// };

module.exports.checkEmailExists = (reqBody) => {
  // findEmail(reqBody);
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result != null) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
  });
  return newUser.save().then((user, err) => {
    if (err) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      // creates variable to return the result of comparing the login form password and the database password
      // "compareSync" is used to compare a non encrypted password from the login form to the encrypted password from the database and returns "true" or "false"
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      // if the password matches
      if (isPasswordCorrect) {
        // Generate an access token
        // it uses the "createAccessToken" method that we defined inm the auth.js file
        // returning an object back to the frontend application is common practice to ensure information is properly labeled.
        return { acess: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

module.exports.getProfile = (reqId) => {
  return User.findById(reqId).then((result, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      result.password = "";
      return result;
    }
  });
};

module.exports.enroll = async (data) => {
  let userEnrollee = await User.findById(data.userId);
  let courseToEnroll = await Course.findById(data.courseId);
  let isEnrolled = courseToEnroll.enrollees.find(
    (user) => user.userId == data.userId
  );

  console.log(userEnrollee);
  console.log(courseToEnroll);
  console.log(isEnrolled);

  if (userEnrollee && courseToEnroll && !isEnrolled) {
    userEnrollee.enrollments.push({ courseId: data.courseId });
    userEnrollee.save();
    courseToEnroll.enrollees.push({ userId: data.userId });
    courseToEnroll.save();
    return true;
  } else {
    return false;
  }
};
