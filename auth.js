const jwt = require("jsonwebtoken");

//Token Creation
const secret = "CourseBookingAPI";
module.exports.createAccessToken = (user) => {
  const data = { id: user._id, email: user.email, isAdmin: user.isAdmin };
  return jwt.sign(data, secret, {});
};

//Token Verification

module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (token !== undefined) {
    //token = token.slice(7, token.length);
    token = token.replace("Bearer ", "");
    console.log(token);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({ auth: "failed" });
      } else {
        next();
      }
    });
  } else {
    return res.send({ auth: "failed" });
  }
};

//Token decryption

module.exports.decode = (token) => {
  token = token.replace("Bearer ", "");
  if (token !== undefined) {
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null;
  }
};
