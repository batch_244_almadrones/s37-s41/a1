const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");
const app = express();
mongoose.connect(
  "mongodb+srv://admin:admin@zuitt-course-booking.rtqczgx.mongodb.net/b244_booking?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

mongoose.connection.once("open", () =>
  console.log("Now connected to the MongoDB Atlas.")
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Will use the defined port number for the application whenever an environment variable is a available or will use the port 4000 if none is defined
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online at port ${process.env.PORT || 4000}`);
});
